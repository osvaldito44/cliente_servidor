all:
	gcc -g -c -Wall servidor.c -o servidor.o
	gcc -g -c -Wall cliente.c -o cliente.o
	gcc -g -Wall servidor.o -o server
	gcc -g -Wall cliente.o -o cliente

clean:
	rm *.o
	rm server
	rm cliente
